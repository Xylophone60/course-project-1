<?php
require "vendor/autoload.php";

Flight::route('/', function(){
    echo 'hello world!';
});

    $loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
    $twigConfig = array(
        // 'cache' => './cache/twig/',
        // 'cache' => false,
    'debug' => true,
);
Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

Flight::map('render', function($template, $data=array()){
    Flight::view()->display($template, $data);
});

Flight::route('/dinosaure/', function(){
    $data = [
        'dinosaure' => getDinosaure(),
    ];
    Flight::render('dinosaure.twig', $data);
});

Flight::route('/dinosaurs/@name', function($name){
    $data = [
        'dinosaure' => getDinosaure($name),
    ];

    Flight::render('dinosaure.twig', $data);
});

Flight::start();
